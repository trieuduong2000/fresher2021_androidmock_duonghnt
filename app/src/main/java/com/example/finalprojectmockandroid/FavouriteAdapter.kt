package com.example.finalprojectmockandroid

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.finalprojectmockandroid.models.MoviesModels
import kotlinx.android.synthetic.main.movie_grid_item.view.*
import kotlinx.android.synthetic.main.movie_item.view.*

class FavouriteAdapter(
    private var movies: List<MoviesModels>,
    private var mvviewOnClickListener: View.OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var typeList: Int = 1

    fun setMovies(mMovies: List<MoviesModels>) {
        movies = mMovies
        notifyDataSetChanged()
    }

    inner class MovieHolderListFavourite(view: View) : RecyclerView.ViewHolder(view) {
        private val IMAGE_BASE = "https://image.tmdb.org/t/p/w500/"
        private var isAdult: Boolean? = false
        private var imageStar: ImageView = itemView.findViewById(R.id.button)

        fun bindMovieList(movie: MoviesModels) {
            itemView.movie_title.text =
                movie.title + "(" + movie.rating + "/" + movie.rating_count + ")"
            itemView.movie_release_date.text = movie.release
            itemView.movie_overview.text = movie.overview
            imageStar.setImageResource(R.drawable.star_on)
            imageStar.setTag(movie)
            imageStar.setOnClickListener(mvviewOnClickListener)
//            itemView.button.setTag(movie)
//            itemView.button.setOnClickListener(mvviewOnClickListener)
            isAdult = movie.adult

            if (isAdult == true) itemView.movie_adult.text = "Adult"
            else if (isAdult == false) itemView.movie_adult.text = ""
            //Still haven't tested Adult because i couldn't find any movies that has "adult":true

            Glide.with(itemView).load(IMAGE_BASE + movie.poster).into(itemView.movie_poster)
        }
    }

    inner class MovieHolderGridFavourite(view: View) : RecyclerView.ViewHolder(view) {
        private val IMAGE_BASE = "https://image.tmdb.org/t/p/w500/"

        @SuppressLint("SetTextI18n")
        fun bindMovieGrid(movie: MoviesModels) {
            itemView.movie_title_grid.text = movie.title
            itemView.movie_release_date_grid.text = movie.release
            itemView.movie_overview_grid.text = movie.overview
            itemView.movie_rating_count.text = "(" + movie.rating + "/" + movie.rating_count + ")"

            Glide.with(itemView).load(IMAGE_BASE + movie.poster).into(itemView.movie_poster_grid)
        }
    }

    fun setType(typeList: Int) {
        this.typeList = typeList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (typeList == 1) {
            return MovieHolderListFavourite(
                LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
            )
        } else {
            return MovieHolderGridFavourite(
                LayoutInflater.from(parent.context).inflate(R.layout.movie_grid_item, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FavouriteAdapter.MovieHolderListFavourite) {
            holder.bindMovieList(movies[position])
        } else if (holder is FavouriteAdapter.MovieHolderGridFavourite)
            holder.bindMovieGrid(movies[position])
    }

    override fun getItemCount(): Int = movies.size

}