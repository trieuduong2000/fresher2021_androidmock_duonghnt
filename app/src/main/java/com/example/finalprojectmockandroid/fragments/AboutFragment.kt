package com.example.finalprojectmockandroid.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.finalprojectmockandroid.R

class AboutFragment : Fragment() {

    private lateinit var lorem: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_about, container, false)
        lorem = view.findViewById(R.id.lorem_ipsum)
        lorem.setText(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ornare purus ac neque posuere, in volutpat odio vulputate. Aenean quis ante laoreet, posuere massa sed, ultricies risus. Morbi eleifend venenatis enim in ornare. Vestibulum bibendum interdum metus, sed sagittis ligula. Sed tristique mauris at ex euismod, quis ornare tortor consectetur. Etiam cursus tempus nunc, quis bibendum risus laoreet id. Fusce in consectetur arcu, et mollis purus.\n" +
                    "\n" +
                    "Pellentesque bibendum enim sed dapibus bibendum. Mauris sit amet eleifend sapien. Suspendisse commodo diam iaculis velit pulvinar, id fermentum justo blandit. Proin quis erat lorem. Praesent mollis ex blandit dolor congue imperdiet. Morbi scelerisque volutpat mi, at posuere neque blandit ut. Pellentesque ut felis mattis, efficitur magna eget, pretium nulla. Curabitur volutpat efficitur lacinia.\n" +
                    "\n" +
                    "Duis a ipsum porttitor, facilisis ipsum in, lacinia dui. Nulla massa tortor, iaculis eu vestibulum ac, molestie auctor nunc. Curabitur ut orci suscipit nunc mattis suscipit. Vivamus vestibulum enim nibh, vitae porta dolor blandit iaculis. Nullam porttitor augue vitae ullamcorper pulvinar. Integer congue elit eleifend diam eleifend, at faucibus purus facilisis. Vivamus tincidunt dui non libero pellentesque, eu maximus nisi lobortis. Sed sagittis purus eget lorem porttitor, et lacinia tortor placerat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc ornare orci ut enim elementum, a imperdiet purus vehicula. Pellentesque neque odio, iaculis vel leo in, blandit vestibulum lacus. Praesent rhoncus, justo a commodo consequat, lectus sapien venenatis mi, eget gravida elit turpis sed orci.\n" +
                    "\n" +
                    "Nulla consectetur enim nec purus aliquet, vitae porttitor ex eleifend. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur et euismod purus. Vivamus finibus blandit velit eget ornare. Fusce fermentum ligula sit amet dui commodo, quis vehicula lacus mollis. Integer et risus et tortor consequat lacinia in sit amet sapien. Fusce fermentum ipsum non est interdum, et porta lacus aliquam. Proin tincidunt, erat nec scelerisque congue, neque quam vulputate nibh, in sollicitudin dui purus et tellus. Donec aliquet massa ut consequat sagittis. Cras vestibulum purus sed suscipit lobortis. Donec nibh metus, cursus eget nulla aliquam, eleifend aliquet turpis. Quisque eu purus eget elit ultrices molestie ut eget magna. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer vitae euismod dolor. Nulla sed elit ut magna ultricies condimentum ut at metus.\n" +
                    "\n" +
                    "Phasellus vestibulum consequat purus non molestie. Suspendisse congue facilisis erat, id tempor dolor. Nullam placerat nulla sed elit lacinia semper. Phasellus finibus dapibus magna, eget vulputate libero ornare at. Vestibulum ligula purus, mollis vitae justo ac, auctor malesuada nisi. Ut facilisis feugiat purus, facilisis tempor orci bibendum vitae. Praesent viverra magna sit amet bibendum finibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam consequat purus vel augue consequat, a suscipit turpis aliquam. Curabitur tempus odio vitae urna molestie, sed accumsan lacus cursus. Nulla facilisi.\n" +
                    "\n" +
                    "Vestibulum imperdiet ante eu massa malesuada egestas. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed efficitur ultrices vehicula. Etiam consequat nec nunc tempor euismod. Maecenas faucibus turpis quis ligula porta, at varius purus vestibulum. Sed sodales ut mauris id egestas. Etiam mi eros, semper vitae vehicula ut, faucibus nec eros. Phasellus tempus, tortor sed consequat consequat, tortor odio imperdiet nisi, nec volutpat elit nulla sit amet diam. Morbi id volutpat sapien, et pulvinar orci.\n" +
                    "\n" +
                    "Ut hendrerit, magna vulputate sodales tincidunt, sapien magna ullamcorper ex, ut iaculis purus diam nec turpis. Phasellus blandit felis et diam tempus iaculis. Pellentesque rhoncus dolor at tellus faucibus vestibulum. Integer ultricies purus fringilla tincidunt luctus. Ut euismod convallis dui sit amet auctor. Mauris felis quam, molestie sed laoreet eu, ultricies vel lorem. Quisque suscipit enim at tortor molestie maximus. Curabitur dignissim augue eget quam dapibus, sit amet ornare dui maximus. Nunc turpis mauris, volutpat sed orci convallis, consequat vestibulum tortor.\n" +
                    "\n" +
                    "Quisque dictum congue aliquet. Nullam hendrerit sollicitudin lectus eget vulputate. Quisque pretium lobortis nibh sed malesuada. Praesent eu lectus pretium, pulvinar diam quis, euismod mi. Sed neque dolor, cursus sed arcu in, commodo pharetra justo. Donec lorem eros, tristique nec cursus eget, dignissim id lectus. Nullam tristique lectus consequat tincidunt posuere. Mauris quis rhoncus leo. Ut efficitur faucibus magna sed placerat. Sed vehicula mattis arcu eget posuere. Mauris suscipit vulputate nulla a dignissim. Quisque sodales, risus ac aliquam placerat, nisl tortor porttitor purus, ut dapibus diam sapien nec nisi. Fusce ipsum justo, gravida non malesuada sed, fermentum non neque.\n" +
                    "\n" +
                    "Donec urna magna, finibus quis metus eget, maximus ornare turpis. Praesent gravida congue vestibulum. Cras at varius ligula, in condimentum est. In pellentesque diam ac odio convallis porta. Mauris nec venenatis mi. Aenean sit amet tellus suscipit tellus elementum fringilla sed non urna. Pellentesque et quam aliquam, molestie tellus vitae, congue lectus. Nulla viverra leo sit amet mi pretium tristique. Praesent nibh dui, lacinia quis eros ut, suscipit porttitor ex. Maecenas ac consequat nulla, quis tincidunt ligula. Fusce rhoncus semper justo, et interdum enim semper rhoncus.\n" +
                    "\n" +
                    "Phasellus nec tellus sodales, sollicitudin metus eget, vestibulum augue. Vivamus vel augue eget erat imperdiet accumsan vitae eget nulla. Etiam fermentum, elit non dictum sodales, nunc metus placerat ex, blandit elementum mi orci nec mauris. Maecenas facilisis massa vitae quam pellentesque ornare. Cras mattis, sem in commodo hendrerit, nunc magna varius nunc, a consequat quam magna lobortis mauris. Donec vitae massa condimentum, facilisis dolor vitae, aliquet ipsum. Fusce venenatis tempor ligula, in consectetur nibh blandit a. Donec nec enim quis nunc sollicitudin fringilla id non neque. Integer maximus dolor nec metus sollicitudin ultricies. Aenean at ornare augue, id eleifend nibh. Donec tincidunt odio eget quam elementum, eu ornare leo rutrum. Vivamus quis mi consectetur, accumsan mi et, rhoncus metus.\n" +
                    "\n" +
                    "Vivamus quis fringilla mauris, vel consectetur erat. Curabitur est sapien, faucibus non ante vitae, faucibus rutrum diam. Donec venenatis purus in orci mattis, eu gravida mi tempor. Morbi ut nisl lectus. Pellentesque lobortis, justo non sagittis imperdiet, sapien mi maximus mauris, nec vulputate augue metus ut dolor. Suspendisse lacinia placerat commodo. Aliquam at blandit purus. Cras lacinia, ex pulvinar vulputate vestibulum, dui eros auctor nisi, sit amet."
        )
        return view
    }
}