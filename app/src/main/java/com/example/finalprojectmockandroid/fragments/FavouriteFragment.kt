package com.example.finalprojectmockandroid.fragments

import android.database.DatabaseErrorHandler
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.finalprojectmockandroid.FavouriteAdapter
import com.example.finalprojectmockandroid.R
import com.example.finalprojectmockandroid.db.MovieDB
import com.example.finalprojectmockandroid.models.MoviesModels
import kotlinx.android.synthetic.main.fragment_favourite.*

class FavouriteFragment(
    private var mvListCallBackListener: MoviesFragment.MovieListCallBackListener
) : Fragment(), View.OnClickListener {

    private lateinit var mvFavouriteModels: MutableList<MoviesModels>
    private lateinit var movieDB: MovieDB
    private lateinit var mvMovieListCallBackListener: MoviesFragment.MovieListCallBackListener
    private var TAG = "FavouriteFragment"
    private var mvNumberFavourite = 0
    private lateinit var linearLayout: LinearLayoutManager
    private lateinit var gridLayout: GridLayoutManager
    private lateinit var recycleView: RecyclerView
    private lateinit var mvFavouriteAdapter: FavouriteAdapter
    //    private lateinit var mvFavouriteListListener: FavouriteListListener

//    fun setFavouriteListener(mvFavouriteListListener: FavouriteListListener)
//    {
//        this.mvFavouriteListListener = mvFavouriteListListener
//    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_favourite, container, false)
        recycleView = view.findViewById(R.id.recycleView_favourite)
        movieDB = MovieDB(activity, MovieDB.MOVIE_TABLE, null, 1, DatabaseErrorHandler {})

        mvFavouriteModels = movieDB.getMovies()

        mvFavouriteAdapter = FavouriteAdapter(mvFavouriteModels, this)
        mvFavouriteAdapter.setMovies(mvFavouriteModels)
        mvNumberFavourite = mvFavouriteModels.size
        Log.d(TAG, "favMovieSize: " + mvFavouriteModels.size)

        linearLayout = LinearLayoutManager(context)
        gridLayout = GridLayoutManager(context, 3)

        recycleView.layoutManager = linearLayout
        mvFavouriteAdapter.setType(1)
        recycleView.setHasFixedSize(true)

        bindData()
        return view
    }

    fun bindData() {
        mvFavouriteModels = movieDB.getMovies()
        mvFavouriteAdapter.setMovies(mvFavouriteModels)
        recycleView.adapter = mvFavouriteAdapter
        recycleView.adapter?.notifyDataSetChanged()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipe_container_favourite.setOnRefreshListener {
            mvFavouriteModels = movieDB.getMovies()
            mvFavouriteAdapter.setMovies(mvFavouriteModels)
            recycleView.adapter = mvFavouriteAdapter
            recycleView.adapter?.notifyDataSetChanged()
            Handler().postDelayed({
                swipe_container_favourite.isRefreshing = false
            }, 3000)
        }
        mvFavouriteAdapter.notifyDataSetChanged()
    }

    fun changeViewList(viewType: Int) {
        if (viewType == 1) {
            recycleView.layoutManager = linearLayout
            recycleView.setHasFixedSize(true)
            mvFavouriteAdapter.setType(1)
            recycleView.adapter = mvFavouriteAdapter
        } else if (viewType == 2) {
            recycleView.layoutManager = gridLayout
            recycleView.setHasFixedSize(true)
            mvFavouriteAdapter.setType(2)
            recycleView.adapter = mvFavouriteAdapter

        }
        mvFavouriteAdapter.notifyItemRangeChanged(0, mvFavouriteAdapter.itemCount)
    }

//    fun reloadData() {
//        mvFavouriteModels.clear()
//        mvFavouriteModels = movieDB.getMovies()
//        mvFavouriteAdapter.updateData(mvFavouriteModels)
//    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.button -> {
                val movies: MoviesModels = v.tag as MoviesModels
                movies.id?.let { movieDB.deleteMoviesModels(it) }
                mvFavouriteModels.remove(movies)
                mvNumberFavourite = mvFavouriteModels!!.size
                mvFavouriteAdapter.notifyDataSetChanged()
                Log.d(TAG, "removedFromDatabase: " + mvNumberFavourite)
//                mvFavouriteListener.onNumberFromFavouriteList(movies)
                mvListCallBackListener.onNumberFavourite(mvFavouriteModels.size)
            }
        }
    }
}