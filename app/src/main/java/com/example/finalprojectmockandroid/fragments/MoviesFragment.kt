package com.example.finalprojectmockandroid.fragments

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.finalprojectmockandroid.MovieAdapter
import com.example.finalprojectmockandroid.R
import com.example.finalprojectmockandroid.db.MovieDB
import com.example.finalprojectmockandroid.models.MoviesModels
import com.example.finalprojectmockandroid.models.MoviesResponse
import com.example.finalprojectmockandroid.services.MovieApiInterface
import com.example.finalprojectmockandroid.services.MovieApiService
import kotlinx.android.synthetic.main.fragment_movies.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoviesFragment(
    private var mvListCallBackListener: MovieListCallBackListener
) : Fragment(), View.OnClickListener, MovieDetailFragment.FavListener {

    private lateinit var mvAdapter: MovieAdapter
    private lateinit var mvModels: MutableList<MoviesModels>
    private lateinit var mvFavouriteModels: List<MoviesModels>
    private lateinit var mvDB: MovieDB
    private lateinit var mvMovieDetailFragment: MovieDetailFragment
    private var mvNumberFavourite = 0

    private var mvMoviePositionLastClick: Int = 0

    private lateinit var linearLayout: LinearLayoutManager
    private lateinit var gridLayout: GridLayoutManager
    private lateinit var recycleView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_movies, container, false)
        mvModels = ArrayList()
        recycleView = view.findViewById(R.id.recycleView)
        mvDB = MovieDB(activity, MovieDB.MOVIE_TABLE, null, 1, {})

        mvFavouriteModels = ArrayList()
        mvFavouriteModels = mvDB.getMovies()

        linearLayout = LinearLayoutManager(context)
        gridLayout = GridLayoutManager(context, 3)
        recycleView.layoutManager = linearLayout
        recycleView.setHasFixedSize(true)
        mvAdapter = MovieAdapter(mvModels, this)
        recycleView.adapter = mvAdapter

        bindData()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        swipe_container.setOnRefreshListener {
            mvAdapter.setMovies(mvModels)
            mvAdapter.notifyDataSetChanged()
            recycleView.adapter = mvAdapter
            Handler().postDelayed({
                swipe_container.isRefreshing = false
            }, 3000)
        }
    }

    fun setFavourite() {
        for (i in 0 until mvModels.size) {
            for (j in 0 until mvFavouriteModels.size) {
                if (mvModels.get(i).id === mvFavouriteModels.get(j).id) {
                    mvModels.get(i).isFavourite = true
                }
            }
        }
    }

    private fun bindData() {
        getMovieData()
        mvAdapter.setType(1)
    }

    fun changeViewList(viewType: Int) {
        if (viewType == 1) {
            recycleView.layoutManager = linearLayout
            recycleView.setHasFixedSize(true)
            getMovieData()
            mvAdapter.setType(1)
            recycleView.adapter = mvAdapter
        } else if (viewType == 2) {
            recycleView.layoutManager = gridLayout
            recycleView.setHasFixedSize(true)
            getMovieData()
            mvAdapter.setType(2)
            recycleView.adapter = mvAdapter

        }
        mvAdapter.notifyItemRangeChanged(0, mvAdapter.itemCount)
    }

    private fun getMovieData() {
        val apiService = MovieApiService.getInstance().create(MovieApiInterface::class.java)
        apiService.getMovieList().enqueue(object : Callback<MoviesResponse> {
            override fun onResponse(
                call: Call<MoviesResponse>?, response: Response<MoviesResponse>?
            ) {
                if (response!!.body().movies != null && !response.body().movies.isEmpty()) {
                    mvModels.addAll(response.body().movies)
                    mvAdapter.setMovies(mvModels)
                    mvAdapter.notifyDataSetChanged()
                    setFavourite()
                    mvNumberFavourite = mvFavouriteModels.size
                    mvListCallBackListener.onNumberFavourite(mvNumberFavourite)
                }
            }

            override fun onFailure(call: Call<MoviesResponse>?, t: Throwable?) {
                Toast.makeText(context, "Could not load data", Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.button -> {
                var movies: MoviesModels = v.tag as MoviesModels
                mvNumberFavourite = mvFavouriteModels.size
                if (movies.isFavourite == true) {
                    if (mvDB.deleteMoviesModels(movies.id)) {
                        movies.isFavourite = false
                        mvNumberFavourite--
                        mvAdapter.notifyDataSetChanged()
                    }
                } else {
                    if (mvDB.addMovies(movies)) {
                        movies.isFavourite = true
                        mvNumberFavourite++
                        mvAdapter.notifyDataSetChanged()
                    }
                }
                loadDataFavourite()
                mvFavouriteModels = mvDB.getMovies()
                mvListCallBackListener.onNumberFavourite(mvNumberFavourite)
            }

            R.id.movie_item -> {
                mvMoviePositionLastClick = v.tag as Int
                val moviesModels: MoviesModels = mvModels.get(mvMoviePositionLastClick)
                mvMovieDetailFragment = MovieDetailFragment(this)
                val bundle = Bundle()
                bundle.putSerializable("nah", moviesModels)
                mvMovieDetailFragment.arguments = bundle
                val transaction: FragmentTransaction =
                    requireActivity().supportFragmentManager!!.beginTransaction()
                transaction.replace(R.id.movie_fragment_layout, mvMovieDetailFragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }
        }
    }

    interface MovieListCallBackListener {
        fun onNumberFavourite(number: Int)
    }

    fun loadDataFavourite() {
        mvFavouriteModels = mvDB.getMovies()
        setFavourite()
        mvAdapter.notifyDataSetChanged()
    }

    override fun FavListener() {
        loadDataFavourite()
        mvListCallBackListener.onNumberFavourite(mvFavouriteModels.size)
    }
}