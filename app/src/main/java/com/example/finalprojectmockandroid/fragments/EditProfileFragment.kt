package com.example.finalprojectmockandroid.fragments

import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.finalprojectmockandroid.R
import com.example.finalprojectmockandroid.utils.BitmapUtils
import com.example.finalprojectmockandroid.utils.PermissionUtils
import com.example.finalprojectmockandroid.utils.SharePreferenceUtils
import java.text.SimpleDateFormat
import java.util.*

class EditProfileFragment(
    private var profileEditListener: ProfileEditListener
) : Fragment(), View.OnClickListener {
    private lateinit var userAvatar: ImageView
    private lateinit var name: EditText
    private lateinit var dob: TextView
    private lateinit var email: EditText
    private lateinit var gender: RadioButton
    private lateinit var btnCancel: Button
    private lateinit var btnDone: Button
    private lateinit var mvBitmap: Bitmap

    var permissions = arrayOf(
        android.Manifest.permission.CAMERA,
        android.Manifest.permission.READ_EXTERNAL_STORAGE,
        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_editprofile, container, false)
        userAvatar = view.findViewById(R.id.pfp)
        name = view.findViewById(R.id.name)
        dob = view.findViewById(R.id.dob)
        email = view.findViewById(R.id.email)
        gender = view.findViewById(R.id.maleButton)
        btnCancel = view.findViewById(R.id.cancelButton)
        btnDone = view.findViewById(R.id.doneButton)

        userAvatar.setOnClickListener {
            if (PermissionUtils.checkPermission(requireContext(), permissions)) {
                selectImage()
            } else {
                PermissionUtils.requestPermission(requireActivity(), permissions, 0)
            }
        }

        dob.setOnClickListener {
            var date = arrayOf(Date())
            var calendar = Calendar.getInstance()
            calendar.time = date[0]
            DatePickerDialog(
                requireActivity(),
                { view, y, m, d ->
                    calendar.set(Calendar.YEAR, y)
                    calendar.set(Calendar.MONTH, m)
                    calendar.set(Calendar.DAY_OF_MONTH, d)
                    var formatDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
                    dob.setText(formatDate.format(calendar.time))
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(
                    Calendar.DAY_OF_MONTH
                )
            ).show()
        }

        btnDone.setOnClickListener {
            SharePreferenceUtils.saveStringPreference(
                requireContext(),
                "pfpKey",
                BitmapUtils.encodeBase64(mvBitmap)
            )
            SharePreferenceUtils.saveStringPreference(
                requireContext(),
                "nameKey",
                name.text.toString().trim()
            )
            SharePreferenceUtils.saveStringPreference(
                requireContext(),
                "emailKey",
                email.text.toString().trim()
            )
            SharePreferenceUtils.saveStringPreference(
                requireContext(),
                "birthdayKey",
                dob.text.toString().trim()
            )
            if (gender.isChecked) {
                SharePreferenceUtils.saveStringPreference(requireContext(), "genderKey", "Male")
            } else {
                SharePreferenceUtils.saveStringPreference(requireContext(), "genderKey", "Female")
            }
            profileEditListener.onDone()
        }

        btnCancel.setOnClickListener {
            profileEditListener.onCancel()
        }
        return view
    }

    override fun onClick(v: View) {
//        when (v.id) {
//            R.id.pfp -> {
//                if(PermissionUtils.checkPermission(requireContext(), permissions)){
//                    selectImage()
//                }else{
//                    PermissionUtils.requestPermission(requireActivity(),permissions,0)
//                }
//            }
//            R.id.dob -> {
//                var date = arrayOf(Date())
//                var calendar = Calendar.getInstance()
//                calendar.time = date[0]
//                DatePickerDialog(requireActivity(),
//                    { view,y,m,d -> calendar.set(Calendar.YEAR, y)
//                        calendar.set(Calendar.MONTH, m)
//                        calendar.set(Calendar.DAY_OF_MONTH, d)
//                        var formatDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
//                        dob.setText(formatDate.format(calendar.time))
//                    }, calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(
//                        Calendar.DAY_OF_MONTH)).show()
//            }
//            R.id.doneButton ->{
//                SharePreferenceUtils.saveStringPreference(requireContext(),"pfpKey", BitmapUtils.encodeBase64(mvBitmap))
//                SharePreferenceUtils.saveStringPreference(requireContext(),"nameKey",name.text.toString().trim())
//                SharePreferenceUtils.saveStringPreference(requireContext(),"emailKey", email.text.toString().trim())
//                SharePreferenceUtils.saveStringPreference(requireContext(),"birthdayKey", dob.text.toString().trim())
//                if (gender.isChecked){
//                    SharePreferenceUtils.saveStringPreference(requireContext(),"genderKey","Male")
//                }else{
//                    SharePreferenceUtils.saveStringPreference(requireContext(),"genderKey","Female")
//                }
//                profileEditListener.onDone()
//            }
//            R.id.cancelButton -> {
//                profileEditListener.onCancel()
//            }
//        }
    }

    private fun selectImage() {
        val options = arrayOf("Take Photo", "Choose from Gallery", "Cancel")
        var builder = AlertDialog.Builder(context)
        builder.setTitle("Add your photo")
        builder.setItems(options) { dialog, item ->
            when (item) {
                0 -> {
                    var intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(intent, 1)
                }
                1 -> {
                    var intent =
                        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    startActivityForResult(intent, 2)
                }
                2 -> {
                    dialog.dismiss()
                }
            }
        }
        builder.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                0 -> {
                    selectImage()
                }
                1 -> {
                    var extras = data!!.extras
                    mvBitmap = extras!!.get("data") as Bitmap
                    userAvatar.setImageBitmap(mvBitmap)
                }
                2 -> {
                    var imagePath = data!!.data
                    var filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                    var cursor = requireContext().contentResolver.query(
                        imagePath!!,
                        filePathColumn,
                        null,
                        null,
                        null
                    )
                    cursor!!.moveToFirst()
                    var columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
                    var picturePath: String = cursor.getString(columnIndex)
                    cursor.close()
                    mvBitmap = BitmapFactory.decodeFile(picturePath)
                    userAvatar.setImageBitmap(mvBitmap)
                }
            }
        }
    }

    interface ProfileEditListener {
        fun onDone()
        fun onCancel()
    }
}