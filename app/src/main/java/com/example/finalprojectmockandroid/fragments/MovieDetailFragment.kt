package com.example.finalprojectmockandroid.fragments

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.finalprojectmockandroid.CastAndCrewAdapter
import com.example.finalprojectmockandroid.R
import com.example.finalprojectmockandroid.constant.APIConstant
import com.example.finalprojectmockandroid.db.MovieDB
import com.example.finalprojectmockandroid.models.*
import com.example.finalprojectmockandroid.services.MovieApiInterface
import com.example.finalprojectmockandroid.services.MovieApiService
import com.example.finalprojectmockandroid.utils.AlarmManagerUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MovieDetailFragment(
    private var mvFavListener: FavListener
) : Fragment() {

    private lateinit var mvPosterDetail: ImageView
    private lateinit var mvTitleDetail: TextView
    private lateinit var mvRatingDetail: TextView
    private lateinit var mvDateDetail: TextView
    private lateinit var mvOverviewDetail: TextView
    private lateinit var mvRecycleView: RecyclerView
    private lateinit var mvReminder: Button
    private lateinit var mvFavouriteButtonDetail: ImageView

    private lateinit var mvModels: MoviesModels
    private lateinit var mvDB: MovieDB
    private lateinit var mvDBReminder: MovieDB
    private lateinit var mvCastModels: List<CastModels>
    private lateinit var mvCrewModels: List<CrewModels>

    private lateinit var mvCastAdapter: CastAndCrewAdapter
    private var mvMovieService: MovieApiInterface? = null

    private var IMAGE_BASE = "https://image.tmdb.org/t/p/w500/"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_movie_detail, container, false)
        mvPosterDetail = view.findViewById(R.id.movie_poster_detail)
        mvTitleDetail = view.findViewById(R.id.movie_title_detail)
        mvRatingDetail = view.findViewById(R.id.movie_rating_detail)
        mvDateDetail = view.findViewById(R.id.movie_date_detail)
        mvOverviewDetail = view.findViewById(R.id.movie_overview_detail)
        mvRecycleView = view.findViewById(R.id.cast_recycler)
        mvReminder = view.findViewById(R.id.btnReminder)
        mvFavouriteButtonDetail = view.findViewById(R.id.button_detail)

        mvCastModels = ArrayList()
        mvCrewModels = ArrayList()
        mvModels = MoviesModels()
        mvDB = MovieDB(activity, MovieDB.MOVIE_TABLE, null, 1, null)
        mvDBReminder = MovieDB(activity, MovieDB.REMINDER_TABLE, null, 1, null)

        var bundle: Bundle? = this.arguments
        if (bundle != null) {
            mvModels = (bundle.getSerializable("nah") as MoviesModels?)!!
        } else {
            Toast.makeText(requireContext(), "Bundle is null", Toast.LENGTH_SHORT)
        }

        mvMovieService = MovieApiService.getInstance().create(MovieApiInterface::class.java)

        mvCastAdapter = CastAndCrewAdapter(requireActivity(), mvCastModels, mvCrewModels)
        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        mvRecycleView.layoutManager = layoutManager
        mvRecycleView.adapter = mvCastAdapter
        //Do ImageStar here
        loadDataMovie(mvModels.id)
        loadDataCast(mvModels.id)

        mvReminder.setOnClickListener {
            doReminder()
        }

        mvFavouriteButtonDetail.setOnClickListener {
            if (mvModels.isFavourite == true) {
                if (mvDB.deleteMoviesModels(mvModels.id)) {
                    mvModels.isFavourite = false
                    mvFavouriteButtonDetail.setImageResource(R.drawable.star_off)
                }
            } else {
                if (mvDB.addMovies(mvModels)) {
                    mvModels.isFavourite = true
                    mvFavouriteButtonDetail.setImageResource(R.drawable.star_on)
                }
            }
            mvFavListener.FavListener()
        }
        return view
    }

    private fun loadDataMovie(id: Int) {
        mvMovieService?.getMovieDetail(id, APIConstant.API_KEY)
            ?.enqueue(object : Callback<MovieDetailResponseModels> {
                override fun onResponse(
                    call: Call<MovieDetailResponseModels>?,
                    response: Response<MovieDetailResponseModels>?
                ) {
                    if (mvModels.isFavourite) {
                        mvFavouriteButtonDetail.setImageResource(R.drawable.star_on)
                    } else {
                        mvFavouriteButtonDetail.setImageResource(R.drawable.star_off)
                    }
                    mvDateDetail.setText(response!!.body().release)
                    mvOverviewDetail.setText(response!!.body().overview)
                    mvTitleDetail.setText(response!!.body().title)
                    activity?.let {
                        Glide.with(it).load(IMAGE_BASE + response.body().poster)
                            .into(mvPosterDetail)
                    }
                    mvRatingDetail.setText("" + response.body().rating)
                }

                override fun onFailure(call: Call<MovieDetailResponseModels>?, t: Throwable?) {}
            })
    }

    private fun doReminder() {
        var value = arrayOf(Date())
        var calendar = Calendar.getInstance()
        calendar.time = value[0]
        DatePickerDialog(requireActivity(),
            { view, y, m, d ->
                calendar.set(Calendar.YEAR, y)
                calendar.set(Calendar.MONTH, m)
                calendar.set(Calendar.DAY_OF_MONTH, d)
                TimePickerDialog(requireActivity(),
                    { view, h, min ->
                        calendar.set(Calendar.HOUR_OF_DAY, h)
                        calendar.set(Calendar.MINUTE, min)
                        value[0] = calendar.time
                        val formateDate = SimpleDateFormat("HH:mm-dd/MM/yyyy", Locale.getDefault())
                        Log.d("DateTimeReminder", "Time in milisec: " + calendar.timeInMillis)
                        Log.d(
                            "DateTimeReminder",
                            "Date time: " + formateDate.format(calendar.timeInMillis)
                        )
                        var reminderTime = calendar.timeInMillis.toString()
                        if (mvDBReminder.getReminderByTimeAndMovieId(mvModels.id!!, reminderTime)
                                .isNotEmpty()
                        ) {
                            Toast.makeText(context, "The reminder existed", Toast.LENGTH_SHORT)
                                .show()
                        } else {
                            mvModels.reminder = reminderTime
                            mvDBReminder.addReminder(mvModels)
                            AlarmManagerUtils.createAlarmManager(
                                requireActivity(),
                                mvModels,
                                mvDBReminder.getReminder().size
                            )
                            Log.d(
                                "DateTimeReminder",
                                "Reminders: " + mvDBReminder.getReminder().size
                            )
                        }
                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true
                ).show()
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        ).show()
    }

    private fun loadDataCast(id: Int) {
        mvMovieService?.getCastDetail(id, APIConstant.API_KEY)
            ?.enqueue(object : Callback<CastAndCrewResponseModels> {
                override fun onResponse(
                    call: Call<CastAndCrewResponseModels>?,
                    response: Response<CastAndCrewResponseModels>?
                ) {
                    if ((response!!.body().cast != null) || response.body().crew != null) {
                        mvCastModels = response.body().cast!!
                        mvCrewModels = response.body().crew!!
                        mvCastAdapter.updateData(mvCastModels, mvCrewModels)
                    }
                }

                override fun onFailure(call: Call<CastAndCrewResponseModels>?, t: Throwable?) {}
            })
    }

    interface FavListener {
        fun FavListener()
    }
}