package com.example.finalprojectmockandroid

import android.graphics.Bitmap
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import com.example.finalprojectmockandroid.fragments.*
import com.example.finalprojectmockandroid.utils.BitmapUtils
import com.example.finalprojectmockandroid.utils.SharePreferenceUtils
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.google.android.material.tabs.TabLayout.TabLayoutOnPageChangeListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MoviesFragment.MovieListCallBackListener,
    EditProfileFragment.ProfileEditListener {

    private lateinit var tabLayout: TabLayout
    private lateinit var adapter: MovieAdapter
    private lateinit var badgeDrawable: BadgeDrawable
    private lateinit var toolbar: Toolbar
    private lateinit var dl: DrawerLayout
    private lateinit var abdt: ActionBarDrawerToggle
    private lateinit var nav_view: NavigationView
    private lateinit var ab: AboutFragment
    private lateinit var mv: MoviesFragment
    private lateinit var fa: FavouriteFragment
    private lateinit var se: SettingsFragment

    private lateinit var userAvatar: ImageView
    private lateinit var nameNav: TextView
    private lateinit var dobNav: TextView
    private lateinit var emailNav: TextView
    private lateinit var genderNav: TextView
    private lateinit var btnEdit: Button
    private lateinit var btnShow: Button
    private lateinit var mvBitmap: Bitmap
    private var mvNumberFavourite = 0
    var icon_state = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar = findViewById(R.id.include)
        toolbar.setTitle("Movies")
        setSupportActionBar(toolbar)
        tabLayoutAndViewPager()
        drawerInit()

        nameNav = findViewById(R.id.nameNav)
        dobNav = findViewById(R.id.dobNav)
        emailNav = findViewById(R.id.emailNav)
        genderNav = findViewById(R.id.genderNav)
        btnEdit = findViewById(R.id.editButtonNav)
        btnEdit.setOnClickListener {
            var editProfile = EditProfileFragment(this)
            var t = supportFragmentManager.beginTransaction()
            t.add(R.id.dl, editProfile)
            t.addToBackStack("DetailFragment")
            t.commit()
        }
        loadUsers()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.appbar_item, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        tabLayout = findViewById(R.id.tabLayout)
        var id = item.itemId
        if (id == R.id.viewSwitch) {
            switchIcon(item)
            return false
        } else if (id == R.id.moviesMenu) {
            tabLayout.getTabAt(0)?.select()
        } else if (id == R.id.favouriteMenu) {
            tabLayout.getTabAt(1)?.select()
        } else if (id == R.id.settingsMenu) {
            tabLayout.getTabAt(2)?.select()
        } else if (id == R.id.aboutMenu) {
            tabLayout.getTabAt(3)?.select()
        }
        return abdt.onOptionsItemSelected(item) || super.onOptionsItemSelected(item)
    }

    private fun tabLayoutAndViewPager() {
        tabLayout = findViewById(R.id.tabLayout)

        mv = MoviesFragment(this)
        fa = FavouriteFragment(this)
        se = SettingsFragment()
        ab = AboutFragment()
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        val adapter = MyAdapter(supportFragmentManager)
        adapter.addFragment(mv, "Movies")
        adapter.addFragment(fa, "Favourite")
        adapter.addFragment(se, "Settings")
        adapter.addFragment(ab, "About")
        viewPager.adapter = adapter
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        tabLayout.setupWithViewPager(viewPager)
        tabLayout.getTabAt(0)!!.setIcon(R.drawable.ic_home)
        tabLayout.getTabAt(1)!!.setIcon(R.drawable.ic_heart)
        tabLayout.getTabAt(2)!!.setIcon(R.drawable.ic_settings)
        tabLayout.getTabAt(3)!!.setIcon(R.drawable.ic_info)

        badgeDrawable = tabLayout.getTabAt(1)!!.orCreateBadge

        viewPager.addOnPageChangeListener(TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                val id = tab.position
                when (id) {
                    0 -> {
                        viewPager.setCurrentItem(0)
                        toolbar.setTitle("Movies")
                        mv.loadDataFavourite()
                    }
                    1 -> {
                        viewPager.setCurrentItem(1)
                        toolbar.setTitle("Favourite")
                        fa.bindData()
                    }
                    2 -> {
                        viewPager.setCurrentItem(2)
                        toolbar.setTitle("Settings")
                    }
                    3 -> {
                        viewPager.setCurrentItem(3)
                        toolbar.setTitle("About")
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    private fun loadUsers() {
        var bitmapBase64 = SharePreferenceUtils.loadStringPreference(this, "pfpKey", "")
        if (!bitmapBase64!!.isEmpty()) {
            userAvatar.setImageBitmap(BitmapUtils.decodeBase64(bitmapBase64))
        }
        var name = SharePreferenceUtils.loadStringPreference(this, "nameKey", "")
        if (!name!!.isEmpty()) {
            nameNav.setText(name)
        }
        var email = SharePreferenceUtils.loadStringPreference(this, "emailKey", "")
        if (!email!!.isEmpty()) {
            emailNav.setText(email)
        }
        var day = SharePreferenceUtils.loadStringPreference(this, "birthdayKey", "")
        if (!day!!.isEmpty()) {
            dobNav.setText(day)
        }
        var gender = SharePreferenceUtils.loadStringPreference(this, "genderKey", "")
        if (!gender!!.isEmpty()) {
            genderNav.setText(gender)
        }
    }

    private fun drawerInit() {
        dl = findViewById(R.id.dl)
        abdt = ActionBarDrawerToggle(this, dl, R.string.Open, R.string.Close)
        abdt.isDrawerIndicatorEnabled = true

        dl.addDrawerListener(abdt)
        abdt.syncState()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        nav_view = findViewById(R.id.nav_view)
    }

    fun switchIcon(item: MenuItem) {
        if (icon_state == 1) {
            mv.changeViewList(2)
            fa.changeViewList(2)
            icon_state = 2
            item.icon = resources.getDrawable(R.drawable.mae)
        } else {
            mv.changeViewList(1)
            fa.changeViewList(1)
            icon_state = 1
            item.icon = resources.getDrawable(R.drawable.tabicon)
        }
    }

    override fun onNumberFavourite(number: Int) {
        mvNumberFavourite = number
        if (number == 0) {
            badgeDrawable.isVisible = false
        } else {
            badgeDrawable.isVisible = true
            badgeDrawable.number = number
        }
    }

    override fun onDone() {
        loadUsers()
        onBackPressed()
    }

    override fun onCancel() {
        onBackPressed()
    }
}