package com.example.finalprojectmockandroid.services

import com.example.finalprojectmockandroid.constant.APIConstant
import com.example.finalprojectmockandroid.models.CastAndCrewResponseModels
import com.example.finalprojectmockandroid.models.MovieDetailResponseModels
import com.example.finalprojectmockandroid.models.MoviesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApiInterface {
    @GET("3/movie/popular?api_key=e7631ffcb8e766993e5ec0c1f4245f93")
    fun getMovieList(): Call<MoviesResponse>

    @GET(APIConstant.MOVIE_DETAIL_URL)
    fun getMovieDetail(
        @Path("movieId") movieId: Int,
        @Query("api_key") api_key: String
    ): Call<MovieDetailResponseModels>

    @GET(APIConstant.CAST_URL)
    fun getCastDetail(
        @Path("movieId") movieId: Int,
        @Query("api_key") api_key: String
    ): Call<CastAndCrewResponseModels>
}