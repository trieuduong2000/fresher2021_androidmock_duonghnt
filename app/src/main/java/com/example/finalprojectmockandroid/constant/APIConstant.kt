package com.example.finalprojectmockandroid.constant

object APIConstant {
    const val BASE_URL = "https://api.themoviedb.org/"
    const val BASE_IMAGE_URL = "https://image.tmdb.org/t/p/w500/"
    const val API_KEY = "e7631ffcb8e766993e5ec0c1f4245f93"
    const val MOVIE_LIST_POPULAR_URL = "3/movie/popular?"
    const val MOVIE_DETAIL_URL = "3/movie/{movieId}?"
    const val CAST_URL = "3/movie/{movieId}/credits?"
}