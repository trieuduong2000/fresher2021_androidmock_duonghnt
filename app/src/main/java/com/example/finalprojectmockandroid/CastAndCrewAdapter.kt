package com.example.finalprojectmockandroid

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.finalprojectmockandroid.models.CastModels
import com.example.finalprojectmockandroid.models.CrewModels
import kotlinx.android.synthetic.main.movie_item_cast.view.*

class CastAndCrewAdapter(
    private var mvContext: Context,
    private var mvCastModels: List<CastModels>,
    private var mvCrewModels: List<CrewModels>
) : RecyclerView.Adapter<CastAndCrewAdapter.CastViewHolder>() {

    private var mvCastModel: CastModels? = null
    private var mvCrewModel: CrewModels? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CastAndCrewAdapter.CastViewHolder {
        val view: View = LayoutInflater.from(mvContext.applicationContext)
            .inflate(R.layout.movie_item_cast, parent, false)
        return CastViewHolder(view)
    }

    override fun onBindViewHolder(holder: CastAndCrewAdapter.CastViewHolder, position: Int) {
        if (mvCastModels != null && mvCastModels.size > 0) {
            if (position < mvCastModels.size) {
                mvCastModel = mvCastModels.get(position)
                holder.bindDataCast(mvCastModel!!)
            } else {
                mvCrewModel = mvCrewModels.get(position - mvCastModels.size)
                holder.bindDataCrew(mvCrewModel!!)
            }
        }
    }

    override fun getItemCount(): Int {
        return (mvCastModels.size + mvCrewModels.size)
    }

    inner class CastViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val IMAGE_BASE = "https://image.tmdb.org/t/p/w500/"

        fun bindDataCast(cast: CastModels) {
            Glide.with(itemView)
                .load(IMAGE_BASE + cast.profilePath)
                .error(R.mipmap.ic_launcher_round)
                .into(itemView.cast_image)
            itemView.cast_text.setText(mvCastModel?.name)
        }

        fun bindDataCrew(crew: CrewModels) {
            Glide.with(itemView)
                .load(IMAGE_BASE + crew.profilePath)
                .error(R.mipmap.ic_launcher_round)
                .into(itemView.cast_image)
            itemView.cast_text.setText(mvCrewModel?.name)
        }
    }

    fun updateData(castModels: List<CastModels>, crewModels: List<CrewModels>) {
        mvCastModels = castModels
        mvCrewModels = crewModels
        notifyDataSetChanged()
    }
}