package com.example.finalprojectmockandroid.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class PermissionUtils {
    companion object {
        fun requestPermission(activity: Activity, permission: Array<String>, requestCode: Int) {
            ActivityCompat.requestPermissions(activity, permission, requestCode)
        }

        fun checkPermission(context: Context, permissions: Array<String>): Boolean {
            var isValid = true
            for (permission in permissions) {
                if (ContextCompat.checkSelfPermission(
                        context,
                        permission
                    ) == PackageManager.PERMISSION_DENIED
                ) {
                    isValid = false
                    break
                }
            }
            return isValid
        }
    }
}