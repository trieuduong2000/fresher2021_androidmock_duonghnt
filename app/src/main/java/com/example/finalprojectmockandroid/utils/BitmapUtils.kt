package com.example.finalprojectmockandroid.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import java.io.ByteArrayOutputStream

class BitmapUtils {
    companion object {
        fun encodeBase64(bitmap: Bitmap): String {
            var byteOutput = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteOutput)
            var byte = byteOutput.toByteArray()
            return Base64.encodeToString(byte, Base64.DEFAULT)
        }

        fun decodeBase64(base64: String): Bitmap {
            var decodeByte = Base64.decode(base64, 0)
            return BitmapFactory.decodeByteArray(decodeByte, 0, decodeByte.size)

        }
    }
}