package com.example.finalprojectmockandroid.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.finalprojectmockandroid.R

class NotificationUtils {
    companion object {
        fun createNotification(
            context: Context,
            notificationId: Int,
            notificationTitle: String,
            notificationContent: String
        ) {
            var builder = NotificationCompat.Builder(context, "channel_id")
                .setSmallIcon(R.drawable.star_off)
                .setContentTitle(notificationTitle)
                .setContentText(notificationContent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            val notificationManagerCompat = NotificationManagerCompat.from(context)
            notificationManagerCompat.notify(notificationId, builder.build())
        }

        fun createNotificationChannel(context: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                var channelName = "channel_name"
                var channelDescription = "channel_description"
                var importance = NotificationManager.IMPORTANCE_DEFAULT
                var channel = NotificationChannel("channel_id", channelName, importance)
                channel.description = channelDescription
                var notificationManager = context.getSystemService(NotificationManager::class.java)
                notificationManager.createNotificationChannel(channel)
            }
        }
    }
}