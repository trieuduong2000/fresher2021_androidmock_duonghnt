package com.example.finalprojectmockandroid.utils

import android.content.Context
import android.preference.PreferenceManager

open class SharePreferenceUtils {
    companion object {
        fun saveStringPreference(context: Context, key: String, value: String) {
            var preferences = PreferenceManager.getDefaultSharedPreferences(context)
            var edit = preferences.edit()
            edit.putString(key, value)
            edit.apply()
        }

        fun loadStringPreference(context: Context, key: String, defaulValue: String): String? {
            var preferences = PreferenceManager.getDefaultSharedPreferences(context)
            return preferences.getString(key, defaulValue)
        }
    }
}