package com.example.finalprojectmockandroid.utils

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.finalprojectmockandroid.models.MoviesModels
import com.example.finalprojectmockandroid.receiver.ReminderBroadcastReceiver

class AlarmManagerUtils {
    companion object {
        fun createAlarmManager(context: Context, movie: MoviesModels, notificationId: Int) {
            val intent = Intent(context, ReminderBroadcastReceiver::class.java)
            val bundle = Bundle()
            bundle.putInt("movie_id", movie.id)
            bundle.putString("time_reminder", movie.reminder)
            bundle.putInt("notification_id", notificationId)
            bundle.putString("notification_title", movie.title)
            bundle.putString("notification_content", movie.release)
            intent.putExtras(bundle)
            var alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            var pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, movie.reminder!!.toLong(), pendingIntent)
        }
    }
}