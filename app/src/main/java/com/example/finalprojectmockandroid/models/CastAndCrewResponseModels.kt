package com.example.finalprojectmockandroid.models

import com.google.gson.annotations.SerializedName

data class CastAndCrewResponseModels(
    @SerializedName("id")
    val id: Int,

    @SerializedName("cast")
    val cast: List<CastModels>? = null,

    @SerializedName("crew")
    val crew: List<CrewModels>? = null
)