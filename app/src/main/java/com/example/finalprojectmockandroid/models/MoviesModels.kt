package com.example.finalprojectmockandroid.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

//@Parcelize
//data class MoviesModels(
//
//    @SerializedName("id")
//    val id: String?,
//
//    @SerializedName("title")
//    val title: String?,
//
//    @SerializedName("poster_path")
//    val poster: String?,
//
//    @SerializedName("vote_average")
//    val rating: String?,
//
//    @SerializedName("vote_count")
//    val rating_count: String?,
//
//    @SerializedName("adult")
//    var adult: Boolean?,
//
//    @SerializedName("release_date")
//    val release: String?,
//
//    @SerializedName("overview")
//    val overview: String?,
//
//    var isFavourite: Boolean?,
//
//    ) : Parcelable {
//        constructor() : this("", "", "", "", "", false, "", "", false)
//    }

data class MoviesModels(
    @SerializedName("id")
    var id: Int,

    @SerializedName("title")
    var title: String,

    @SerializedName("poster_path")
    var poster: String,

    @SerializedName("overview")
    var overview: String,

    @SerializedName("release_date")
    var release: String,

    @SerializedName("vote_average")
    var rating: String,

    @SerializedName("vote_count")
    var rating_count: String,

    @SerializedName("adult")
    var adult: Boolean,

    var isFavourite: Boolean,
    var reminder: String? = null
) : Serializable {
    constructor() : this(0, "", "", "", "", "", "", false, false, "")
}
