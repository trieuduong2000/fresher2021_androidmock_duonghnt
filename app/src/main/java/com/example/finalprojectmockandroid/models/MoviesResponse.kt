package com.example.finalprojectmockandroid.models

import com.google.gson.annotations.SerializedName

data class MoviesResponse(
    @SerializedName("results")
    val movies: List<MoviesModels>
)
