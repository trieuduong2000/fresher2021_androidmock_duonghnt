package com.example.finalprojectmockandroid.models

import com.google.gson.annotations.SerializedName

data class MovieDetailResponse(
    @SerializedName("object")
    val movies: List<MovieDetailResponseModels>
)