package com.example.finalprojectmockandroid.models

import com.google.gson.annotations.SerializedName

data class MovieDetailResponseModels(
    @SerializedName("id")
    var id: Int,

    @SerializedName("title")
    var title: String,

    @SerializedName("poster_path")
    var poster: String,

    @SerializedName("overview")
    var overview: String,

    @SerializedName("release_date")
    var release: String,

    @SerializedName("vote_average")
    var rating: String,

    @SerializedName("vote_count")
    var rating_count: String,

    @SerializedName("adult")
    var adult: Boolean,

    var isFavourite: Boolean
)