package com.example.finalprojectmockandroid.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.finalprojectmockandroid.utils.NotificationUtils

class ReminderBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        var bundle = intent.extras
        if (bundle != null) {
            var movieId = bundle.getInt("movie_id")
            var timeReminder = bundle.getString("reminder_time")
            var notificationId = bundle.getInt("notification_id")
            var notificationTitle = bundle.getString("notification_title")
            var notificationContent = bundle.getString("notification_content")
            NotificationUtils.createNotificationChannel(context)
            NotificationUtils.createNotification(
                context,
                notificationId,
                notificationTitle!!,
                notificationContent!!
            )
        }
    }
}