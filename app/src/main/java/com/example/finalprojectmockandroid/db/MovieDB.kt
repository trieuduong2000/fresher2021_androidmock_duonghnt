package com.example.finalprojectmockandroid.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.DatabaseErrorHandler
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.finalprojectmockandroid.models.MoviesModels

class MovieDB(
    context: Context?,
    name: String?,
    factory: SQLiteDatabase.CursorFactory?,
    version: Int,
    errorHandler: DatabaseErrorHandler?
) : SQLiteOpenHelper(context, name, factory, version, errorHandler) {

    companion object {
        val MOVIE_TABLE = "movie_table"
        val MOVIE_ID = "movie_id"
        val MOVIE_NAME = "movie_name"
        val MOVIE_OVERVIEW = "movie_overview"
        val MOVIE_IMAGE = "movie_image"
        val MOVIE_DATE = "movie_release"
        val MOVIE_RATING = "movie_rating"
        val MOVIE_RATING_COUNT = "movie_rating_count"
        val MOVIE_ADULT = "movie_adult"

        val REMINDER_TABLE = "reminder_table"
        val REMINDER_TIME = "reminder_time"

        val TAG = "MovieDB"
    }

    override fun onCreate(p0: SQLiteDatabase) {
        val favourite_table = String.format(
            "CREATE TABLE " + MOVIE_TABLE + " (" +
                    MOVIE_ID + " INTEGER PRIMARY KEY," +
                    MOVIE_NAME + " TEXT," +
                    MOVIE_IMAGE + " TEXT," +
                    MOVIE_OVERVIEW + " TEXT," +
                    MOVIE_DATE + " TEXT," +
                    MOVIE_RATING + " TEXT," +
                    MOVIE_RATING_COUNT + " TEXT," +
                    MOVIE_ADULT + " TEXT," +
                    "UNIQUE(movie_id))"
        )
        var reminder_table = String.format(
            "CREATE TABLE " + REMINDER_TABLE + " (" +
                    MOVIE_ID + " INTEGER," +
                    MOVIE_NAME + " TEXT," +
                    MOVIE_IMAGE + " TEXT, " +
                    MOVIE_OVERVIEW + " TEXT, " +
                    MOVIE_DATE + " TEXT, " +
                    MOVIE_RATING + " TEXT, " +
                    MOVIE_RATING_COUNT + " TEXT, " +
                    MOVIE_ADULT + " TEXT," +
                    REMINDER_TIME + " TEXT)"

        )
        p0.execSQL(favourite_table)
        p0.execSQL(reminder_table)
    }

    override fun onUpgrade(p0: SQLiteDatabase, p1: Int, p2: Int) {
        val drop_favourite = String.format("DROP TABLE IF EXISTS %s", MOVIE_TABLE)
        val drop_reminder = String.format("DROP TABLE IF EXISTS %s", REMINDER_TABLE)
        p0.execSQL(drop_favourite)
        p0.execSQL(drop_reminder)
        onCreate(p0)
    }

    fun addMovies(movies: MoviesModels): Boolean {
        val check: Long = -1
        val db: SQLiteDatabase = this.writableDatabase
        val values = ContentValues()
        values.put(MOVIE_ID, movies.id)
        values.put(MOVIE_NAME, movies.title)
        values.put(MOVIE_IMAGE, movies.poster)
        values.put(MOVIE_OVERVIEW, movies.overview)
        values.put(MOVIE_DATE, movies.release)
        values.put(MOVIE_RATING, movies.rating)
        values.put(MOVIE_RATING_COUNT, movies.rating_count)
        values.put(MOVIE_ADULT, movies.adult.toString())
        val rowCount: Long = db.insert(MOVIE_TABLE, null, values)
        db.close()
        return rowCount != check
    }

    fun addReminder(movies: MoviesModels): Boolean {
        val check: Long = -1
        val db: SQLiteDatabase = writableDatabase
        val values = ContentValues()
        values.put(MOVIE_ID, movies.id)
        values.put(MOVIE_NAME, movies.title)
        values.put(MOVIE_IMAGE, movies.poster)
        values.put(MOVIE_OVERVIEW, movies.overview)
        values.put(MOVIE_DATE, movies.release)
        values.put(MOVIE_RATING, movies.rating)
        values.put(MOVIE_RATING_COUNT, movies.rating_count)
        values.put(MOVIE_ADULT, movies.adult)
        values.put(REMINDER_TIME, movies.reminder)
        val rowCount: Long = db.insert(REMINDER_TABLE, null, values)
        db.close()
        return rowCount != check
    }

    fun getMovies(): MutableList<MoviesModels> {
        val movie: MutableList<MoviesModels> = ArrayList()
        val query: String = "SELECT * FROM " + MOVIE_TABLE

        val db: SQLiteDatabase = this.readableDatabase
        val cursor: Cursor = db.rawQuery(query, null)
        cursor.moveToFirst()

        while (!cursor.isAfterLast) {
            val movieModel = MoviesModels(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getString(6),
                cursor.getString(7).toBoolean(), true
            )
            movie.add(movieModel)
            cursor.moveToNext()
        }
        Log.d(TAG, "getMovies: " + movie.size)
        return movie
    }

    fun getReminder(): List<MoviesModels> {
        val movie: MutableList<MoviesModels> = ArrayList()
        val query: String = "SELECT * FROM " + REMINDER_TABLE
        val db: SQLiteDatabase = this.readableDatabase
        val cursor: Cursor = db.rawQuery(query, null)
        cursor.moveToFirst()

        while (!cursor.isAfterLast) {
            val reminderModel = MoviesModels(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getString(6),
                cursor.getString(7).toBoolean(),
                false,
                cursor.getString(8)
            )
            movie.add(reminderModel)
            cursor.moveToNext()
        }
        return movie
    }

    fun getReminderByTimeAndMovieId(movieId: Int, reminderTime: String): List<MoviesModels> {
        val movie: MutableList<MoviesModels> = ArrayList()
        val query: String = "SELECT * FROM " + REMINDER_TABLE +
                " WHERE " + MOVIE_ID +
                " = ? AND " + REMINDER_TIME +
                " = ?"

        val db: SQLiteDatabase = this.readableDatabase
        val cursor: Cursor = db.rawQuery(query, arrayOf(movieId.toString(), reminderTime))
        cursor.moveToFirst()

        while (!cursor.isAfterLast) {
            val reminderModel = MoviesModels(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getString(6),
                cursor.getString(7).toBoolean(),
                false,
                cursor.getString(8)
            )
            movie.add(reminderModel)
            cursor.moveToNext()
        }
        return movie
    }

    fun deleteMoviesModels(movieId: Int?): Boolean {
        val check: Long = -1
        val db: SQLiteDatabase = this.writableDatabase
        val rowCount =
            db.delete(MOVIE_TABLE, MOVIE_ID + " =?", arrayOf(java.lang.String.valueOf(movieId)))
                .toLong()
        db.close()
        return rowCount != check
    }
}